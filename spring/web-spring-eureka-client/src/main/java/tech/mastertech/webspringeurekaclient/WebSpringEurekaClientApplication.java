package tech.mastertech.webspringeurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WebSpringEurekaClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSpringEurekaClientApplication.class, args);
	}

}
