package tech.mastertech.webspringeurekaclient01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WebSpringEurekaClient01Application {

	public static void main(String[] args) {
		SpringApplication.run(WebSpringEurekaClient01Application.class, args);
	}

}
