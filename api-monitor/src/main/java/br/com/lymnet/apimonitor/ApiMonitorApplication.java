package br.com.lymnet.apimonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMonitorApplication.class, args);
	}

}
